Kaniko is a fussy beast. It won't attempt to build your image until it
knows it'll be able to push it. This is frustrating when you want to locally
test your image build with Kaniko and don't care about pushing the image.

This repo runs two containers:
- kaniko
- a docker registry

You can shell into the kaniko container, with your Dockerfile mounted in, and
run a build that'll be pushed to the registry container. Then when you're done
testing, just throw it all away.

# How to use it
Requirements: I wrote this using docker and docker-compose.
```
$ docker-compose --version
Docker Compose version 2.5.1
$ docker --version
Docker version 20.10.16, build aa7e414fdc
```
You can probably use Kubernetes, but I didn't test that.

1. clone this repo
1. start the stack
    ```bash
    cd kaniko-sandbox/
    docker-compose up -d
    ```
1. shell into the kaniko container
    ```bash
    docker-compose exec -it kaniko sh
    ```
1. run the build
    ```bash
    /kaniko/executor \
      --context=. \
      --dockerfile=Dockerfile \
      --destination=reg.local:5000/blah:test \
      --build-arg SOME_VAR=banana,A_SECOND_VAR=wombat
    ```
1. here's the output for a successful build
    ```
    INFO[0000] Retrieving image manifest alpine:3.14
    INFO[0000] Retrieving image alpine:3.14 from registry index.docker.io
    INFO[0001] Built cross stage deps: map[]
    INFO[0001] Retrieving image manifest alpine:3.14
    INFO[0001] Returning cached image manifest
    INFO[0001] Executing 0 build triggers
    INFO[0001] Unpacking rootfs as cmd RUN echo "printing: $SOME_VAR, $FROM_IMAGE" requires it.
    INFO[0001] ARG SOME_VAR
    INFO[0001] ENV FROM_IMAGE=heyhey
    INFO[0001] RUN echo "printing: $SOME_VAR, $FROM_IMAGE"
    INFO[0001] Taking snapshot of full filesystem...
    INFO[0001] cmd: /bin/sh
    INFO[0001] args: [-c echo "printing: $SOME_VAR, $FROM_IMAGE"]
    INFO[0001] Running: [/bin/sh -c echo "printing: $SOME_VAR, $FROM_IMAGE"]
    printing: banana, heyhey
    INFO[0001] Taking snapshot of full filesystem...
    INFO[0001] No files were changed, appending empty layer to config. No layer added to image.
    INFO[0001] RUN sh -c 'env | grep SOME_'
    INFO[0001] cmd: /bin/sh
    INFO[0001] args: [-c sh -c 'env | grep SOME_']
    INFO[0001] Running: [/bin/sh -c sh -c 'env | grep SOME_']
    SOME_VAR=banana
    INFO[0001] Taking snapshot of full filesystem...
    INFO[0001] No files were changed, appending empty layer to config. No layer added to image.
    INFO[0001] COPY blah.txt .
    INFO[0001] Taking snapshot of files...
    INFO[0001] RUN cat blah.txt
    INFO[0001] cmd: /bin/sh
    INFO[0001] args: [-c cat blah.txt]
    INFO[0001] Running: [/bin/sh -c cat blah.txt]
    1234
    INFO[0001] Taking snapshot of full filesystem...
    INFO[0001] Pushing image to reg.local:5000/blah:test
    INFO[0002] Pushed reg.local:5000/blah@sha256:395036f85592b119e7934011b270c958e8e34e9f56c691bf1d119ae3698c9ca6
    ```
1. exit the kaniko container
    ```bash
    exit
    ```
1. clean up by destroying the stack
    ```bash
    docker-compose down
    ```

Interesting thing about the included sample:
- using build-args
- including other files

You can modify this sample to test building you image.
